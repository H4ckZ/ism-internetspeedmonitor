# ISM-InternetSpeedMonitor
## Description
Monitor and visualize your internet speed using grafana. All running inside a docker-container.

## Setup
For an initial setup clone this repository
```bash
git clone https://gitlab.com/H4ckZ/ism-internetspeedmonitor.git
```
Then start the script for the first time using the following command:
```bash
./run-speedtest.sh
```
The script will setup the directory structure and pull the speedtest executable from [Sivel](https://github.com/sivel).

For continuous measurement you should add the following line to your crontab (command: ```crontab -e```)
```bash
0 * * * * /bin/bash /path/to/the/repo/run-speedtest.sh
```

## ToDo
 - ~~Run speedtest-cli inside a docker-container (docker-compose) and save csv-formatted data to a shared directory~~
 - Run Grafana inside a docker-container (docker-compose)
 - Import generated csv data into grafana
