#!/bin/bash

generateStructure() {
    if [ -d ./data ]
    then 
        echo "data directory found"
    else
        mkdir -p ./data
    fi
}

getSpeedtestCli() {
    if [ -f ./scripts/speedtest-cli ]
    then
        echo "speedtest-cli found"
    else
        cd ./scripts
        curl -Lo speedtest-cli https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py
        chmod +x speedtest-cli
        cd ..
    fi
}

runContainer() {
    docker-compose up -d
}

generateStructure
getSpeedtestCli
runContainer