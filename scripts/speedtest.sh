#!/bin/bash

executable="/scripts/speedtest-cli"
runtime="python3"
param="--server 818 --simple"
datadir="/data"
logfile="$datadir/speedtest.log"
timestamp=$(date +%Y-%m-%d-%H-%M)

starttest() {
    echo "performing speedtest"
    echo "$timestamp;$($runtime $executable $param | sed ':a;N;$!ba;s/\n/; /g' | cut -f2,3,5,6,8,9 -d' ' | sed 's/; /;/g')" | tee -a $logfile
    echo "speedtest done"
}

echo "run-speedtest.sh started"
echo "checking for logfile $logfile"
if [ -f $logfile ]
then
    echo "logfile exists"
    starttest
else
    echo "logfile not found"
    echo "creating logfile $logfile"
    touch $logfile
    echo "Timestamp;Ping;Download;Upload" > $logfile
    starttest
fi

echo "run-speedtest.sh ended"